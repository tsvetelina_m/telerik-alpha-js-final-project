import React, { useState, useEffect } from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { auth } from './config/firebase-config';
import { useAuthState } from 'react-firebase-hooks/auth';
import { getUserData } from './services/users.services';
import AppContext from './providers/AppContext';
import Dashboard from './views/Dashboard/Dashboard';
import PublicRoute from './hoc/PublicRoute/PublicRoute';
import ProtectedRoute from './hoc/ProtectedRoute/ProtectedRoute';
import { getLiveUserData } from './services/users.services';
import PublicPart from './views/PublicPart/PublicPart';
import { ThemeProvider } from '@mui/material';
import { createTheme } from '@mui/material';

const customTheme = createTheme({
  palette: {
    primary: {
      main: '#00cec3',
      contrastText: '#fff',
    },
    secondary: {
      main: '#066a73',
    },
    light: {
      main: '#fff',
      contrastText: '#00cec3',
    },
  },
});

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });
  const [user, loading] = useAuthState(auth);
  const [error, setError] = useState(true);

  useEffect(() => {
    if (user === null) return;
    // if (user.emailVerified === false) return;
    setError(false);

    getUserData(user.uid)
        .then((snapshot) => {
          if (!snapshot.exists()) {
            throw new Error('Something went wrong!');
          }

          setAppState({
            user,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          });
        })
        .catch((e) => console.error(e));
  }, [user]);

  useEffect(() => {
    if (!appState.userData) return;
    const unsubscribe = getLiveUserData(appState?.userData?.username, (snapshot) => {
      const user = snapshot.val();
      setAppState((prev) => ({ ...prev, userData: user }));
    });

    return unsubscribe;
  }, [appState.user]);

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <ThemeProvider theme={customTheme}>
          <div className="App">
            <Routes>
              <Route
                path="/*"
                element={
                  <PublicRoute loading={loading} user={user} error={error}>
                    <PublicPart error={error} />
                  </PublicRoute>
                }
              />
              <Route
                path="dashboard/*"
                element={
                  <ProtectedRoute loading={loading} user={user} error={error}>
                    <Dashboard />
                  </ProtectedRoute>
                }
              />
            </Routes>
          </div>
        </ThemeProvider>
      </AppContext.Provider>
    </ BrowserRouter>
  );
}

export default App;
