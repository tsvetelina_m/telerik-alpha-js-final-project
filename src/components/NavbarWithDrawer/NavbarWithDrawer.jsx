import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { Avatar, Button, Menu, MenuItem, Tooltip } from '@mui/material';
import AppContext from '../../providers/AppContext';
import NotificationsList from '../NotificationsList/NotificationsList';
import { useNavigate } from 'react-router-dom';
import { logoutUser } from '../../services/auth.services';
import { Collapse } from '@mui/material';
import {
  ExpandLess,
  ExpandMore,
  GroupsOutlined,
  HomeOutlined,
  KeyboardArrowDown,
  ReviewsOutlined,
  TextSnippetOutlined,
} from '@mui/icons-material';

const drawerWidth = 250;

const openedMixin = (theme) => ({
  border: 'none',
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  border: 'none',
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  backgroundColor: '#fafbfb',
  color: '#949db2',
  boxShadow: 'none',
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}));

const data = [
  { icon: <HomeOutlined />, label: 'Dashboard' },
  { icon: <TextSnippetOutlined />, label: 'Work Items' },
  { icon: <ReviewsOutlined />, label: 'Review Requests' },
  { icon: <GroupsOutlined />, label: 'Teams' },
];

export default function NavbarWithDrawer({ children }) {
  const { userData } = React.useContext(AppContext);
  const [_, setAppState] = React.useState({
    user: null,
    userData: null,
  });
  const theme = useTheme();
  const [openItem, setOpenItem] = React.useState({
    'Teams': false,
    'Work Items': false,
    'Dashboard': false,
    'Review Requests': false,
  });
  const [isActive, setIsActive] = React.useState({
    'Teams': false,
    'Work Items': false,
    'Dashboard': true,
    'Review Requests': false,
  });
  const [open, setOpen] = React.useState(false);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const navigate = useNavigate();

  const subMenu = {
    'Teams': [
      ['Create a team', 'create-team'],
      ['My teams', 'my-teams'],
    ],
    'Work Items': [
      ['Create a work item', 'submit-item'],
      ['View work items', 'view-work-items'],
    ],
    'Review Requests': [
      ['My review requests', 'view-review-requests'],
    ],
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
    setOpenItem({
      'Teams': false,
      'Work Items': false,
      'Dashboard': false,
      'Review Requests': false,
    });
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleLogoutClick = () => {
    logoutUser()
        .then(() => {
          setAppState({ user: null, userData: null });
        })
        .catch(() => console.error);
    handleCloseUserMenu();
  };

  const handleProfileClick = () => {
    handleCloseUserMenu();

    return navigate('profile');
  };

  const handleItemClick = (link) => {
    setIsActive({
      'Teams': false,
      'Work Items': false,
      'Dashboard': false,
      'Review Requests': false,
    });

    if (link === 'Dashboard') {
      setIsActive((prev) => ({ ...prev, [link]: true }));

      return navigate('/dashboard');
    }

    if (!open) {
      handleDrawerOpen();
    }
    const linkOpenState = !openItem[link];

    setIsActive((prev) => ({ ...prev, [link]: true }));

    return setOpenItem((prev) => ({ ...prev, [link]: linkOpenState }));
  };

  return userData === null ? null : (
    <Box sx={{
      display: 'flex', height: '100vh' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Box
            sx={{
              flexGrow: 0,
              display: 'flex',
              alignItems: 'center',
              ml: 'auto',
            }}
          >
            <NotificationsList />
            <Tooltip title="Open settings">
              <Button onClick={handleOpenUserMenu} sx={{ p: 1, ml: 2, color: '#949db2' }}
                endIcon={<KeyboardArrowDown />}>
                <Avatar
                  alt=""
                  src={userData.avatarUrl}
                  sx={{ width: 35, height: 35 }}
                />
                {userData !== null ? (
                  <Typography
                    variant="body1"
                    component="span"
                    sx={{ ml: 2, textTransform: 'lowercase' }}
                  >
                    {userData.username}
                  </Typography>
                ) : null}
              </Button>
            </Tooltip>
            <Menu
              sx={{ mt: '40px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              <MenuItem onClick={handleProfileClick}>
                <Typography textAlign="center">Profile</Typography>
              </MenuItem>
              <MenuItem onClick={handleLogoutClick}>
                <Typography textAlign="center">Logout</Typography>
              </MenuItem>
            </Menu>
          </Box>
        </Toolbar>
      </AppBar>
      <Drawer variant="permanent" open={open} >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <List sx={{ padding: 0 }}>
          {data.map((item) => (
            <ListItem key={item.label} disablePadding sx={{ display: 'block' }}>
              <Tooltip
                placement="right"
                title={ !userData.teams &&
                    (item.label === 'Work Items' ||
                    item.label === 'Review Requests') ?
                    `You need to join or create a team to view/add work items and create requests`:
                    ''}
              >
                <span>
                  <ListItemButton
                    onClick={() => handleItemClick(item.label)}
                    sx={{
                      minHeight: 48,
                      justifyContent: open ? 'initial' : 'center',
                      backgroundColor: isActive[item.label] ? '#00cec3' : 'inherit',
                      color: isActive[item.label] ? 'white' : 'inherit',
                      px: 2.5,
                    }}
                    disabled={!userData.teams &&
                            (item.label === 'Work Items' ||
                            item.label === 'Review Requests')
                    }
                  >
                    <ListItemIcon
                      sx={{
                        color: isActive[item.label] ? 'white' : '#949db2',
                        minWidth: 0,
                        mr: open ? 3 : 'auto',
                        justifyContent: 'center',
                      }}
                    >
                      {item.icon}
                    </ListItemIcon>
                    <ListItemText
                      primary={item.label}
                      sx={{ opacity: open ? 1 : 0 }}
                    />
                    {open && item.label !== 'Dashboard' ? (
                  openItem[item.label] ? (
                    <ExpandLess />
                  ) : (
                    <ExpandMore />
                  )
                ) : null}
                  </ListItemButton>
                </span>
              </Tooltip>
              <Collapse
                in={open && openItem[item.label]}
                timeout="auto"
                unmountOnExit
              >
                {subMenu[item.label]?.map(([menu, nav]) => {
                  return (
                    <List component="div" key={menu} disablePadding>
                      <ListItemButton
                        sx={{ pl: 4 }}
                        onClick={() => navigate(nav)}
                      >
                        <ListItemText primary={menu} />
                      </ListItemButton>
                    </List>
                  );
                })}
              </Collapse>
            </ListItem>
          ))}
        </List>
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1, p: 3, backgroundColor: '#fafbfb', height: '110vh' }}>
        <DrawerHeader />
        {children}
      </Box>
    </Box>
  );
}
