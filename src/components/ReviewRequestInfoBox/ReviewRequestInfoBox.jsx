import React from 'react';
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Box, Chip } from '@mui/material';
import parse from 'html-react-parser';
import workItemStatusEnum from '../../enums/work-item-status';

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  'border': `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  'backgroundColor':
    theme.palette.mode === 'dark' ?
      'rgba(255, 255, 255, .05)' :
      'rgba(0, 0, 0, .03)',
  'flexDirection': 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

const ReviewRequestInfoBox = ({ header, reviewRequest, reviewers }) => {
  return ( <Accordion sx={{ textAlign: 'left' }}>
    <AccordionSummary sx={{ bgcolor: '#066a73', color: 'white' }}
      expandIcon={<ExpandMoreIcon sx={{ color: 'white' }} />}
      aria-controls="panel1a-content"
      id="panel1a-header"
    >
      <Typography>{header}</Typography>
    </AccordionSummary>
    <AccordionDetails>
      <Typography variant="h6" component="h6">
        {reviewRequest.title}
      </Typography>
      <Typography variant="body2" component="div">
        {parse(reviewRequest.description)}
      </Typography>
      {Object.keys(reviewRequest.attachments).length === 0 ? null : (
          <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
            <Typography variant="body1" component="div">
              Attachments:
            </Typography>
            {Object.values(reviewRequest.attachments).map((value) => {
              return (
                <Chip
                  component="a"
                  target="_blank"
                  href={value.url}
                  key={value.url}
                  color="primary"
                  variant="outlined"
                  sx={{ ml: 2 }}
                  label={value.name}
                  clickable
                  download
                />
              );
            })}
          </Box>
      )}
      <Typography variant="subtitle2" component="div">
        {'Reviewers: ' + (Object.keys(reviewers).join(', '))}
      </Typography>
      <Typography variant="subtitle2" component="div">
        Accepted by: {Object.values(reviewers)
            .filter((status) => status === workItemStatusEnum.Accepted).length}
        /{Object.keys(reviewers).length}
        {' reviewers'}
      </Typography>
    </AccordionDetails>
  </Accordion>);
};

export default ReviewRequestInfoBox;
