import React, { useEffect, useState } from 'react';
import {
  Tab,
  Box,
  List,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  TableContainer,
  TableCell,
  TableBody,
  TableHead,
  TableRow,
  Table,
  Button,
  ListItem,
  ListItemText,
  Chip,
  Divider,
} from '@mui/material';
import { useContext } from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AppContext from '../../providers/AppContext';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import Moment from 'react-moment';
import {
  getAllWorkItems, getWorkItemsByAuthor } from '../../services/work-items.services';
import { useNavigate } from 'react-router-dom';

const ViewReviewRequests = () => {
  const { userData } = useContext(AppContext);
  const [items, setItems] = useState([]);
  const [reviewItems, setReviewItems] = useState([]);
  const [myItems, setMyItems] = useState([]);
  const [value, setValue] = useState('0');
  const navigate = useNavigate();

  useEffect(() => {
    if (!userData) return;

    getWorkItemsByAuthor(userData.username)
        .then((res) => setMyItems(res.sort((a, b) => a.createdOn - b.createdOn)))
        .catch(console.error);

    getAllWorkItems()
        .then((res) => {
          const reviewerFiltered = res
              .filter((item) => item.reviewers[userData.username])
              .sort((a, b) => a.createdOn - b.createdOn);
          setReviewItems(reviewerFiltered);
          setItems(res);
        })
        .catch(console.error);
  }, [userData]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleDetailsClick = (id) => {
    navigate(`../work-item/${id}`);
  };

  return (
    <Box sx={{ width: '1180px', ml: 'auto', mr: 'auto' }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange}>
            <Tab label="My Review Requests" value='0' />
            <Tab label="Items to Review" value='1' />
            <Tab label="Review requests by team" value='2' />
          </TabList>
        </Box>
        <TabPanel value='0' index={0}>
          <List>
            {
              myItems.filter((item) => item.reviewRequest).length === 0 ?
              'You have not created any review requests yet' :
              myItems
                  .filter((item) => item.reviewRequest)
                  .map((itemObj, i) => {
                    return (
                      <Box key={itemObj.id}>
                        <ListItem
                        >
                          <ListItemText
                            primary={<Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="h6"
                              color="text.primary"
                            >
                              {`Request Title: ${itemObj.reviewRequest.title}`}
                            </Typography>}
                            secondary={
                              <React.Fragment>
                                <Typography
                                  sx={{ display: 'inline' }}
                                  component="span"
                                  variant="body2"
                                  color="text.primary"
                                >
                                  Created on: {
                                    <Moment format="DD/MM/YYYY">
                                      {itemObj.reviewRequest.createdOn}
                                    </Moment>
                                  }
                                </Typography>
                                <Button sx={{ display: 'block' }}
                                  onClick={() => handleDetailsClick(itemObj.id)}
                                >
                              View details
                                </Button>
                              </React.Fragment>
                            }>
                          </ListItemText>
                          <Chip label={itemObj.status} color="primary" sx={{ color: 'white' }} />
                        </ListItem>
                        {i < myItems.filter((item) => item.reviewRequest).length - 1 ?
                        (<Divider variant="middle" component="li" />) :
                        null}
                      </Box>
                    );
                  })
            }
          </List>
        </TabPanel>
        <TabPanel value='1' index={1}>
          <List className='review-items-tab'>
            { reviewItems.length === 0 ?
            'No items for review' :
              reviewItems.map((itemObj, i) => {
                return (
                  <Box key={itemObj.id}>
                    <ListItem
                    >
                      <ListItemText
                        primary={<Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="h6"
                          color="text.primary"
                        >
                          {itemObj.title}
                        </Typography>}
                        secondary={
                          <React.Fragment>
                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                                  Created on: {
                                <Moment format="DD/MM/YYYY">
                                  {itemObj.reviewRequest.createdOn}
                                </Moment>
                              }
                            </Typography>
                            <Button sx={{ display: 'block' }}
                              onClick={() => handleDetailsClick(itemObj.id)}
                            >
                              View details
                            </Button>
                          </React.Fragment>
                        }>
                      </ListItemText>
                      <Chip label={itemObj.status} color="primary" sx={{ color: 'white' }} />
                    </ListItem>
                    {i < myItems.filter((item) => item.reviewRequest).length - 1 ?
                        (<Divider variant="middle" component="li" />) :
                        null}
                  </Box>
                );
              })
            }
          </List>
        </TabPanel>
        <TabPanel value='2' index={2}>
          <Box className='team-review-requests'>
            {
              userData?.teams ?
              Object.entries(userData.teams).map(([teamId, teamName]) => {
                return (
                  <Accordion key={teamId} >
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography>{teamName}</Typography>
                    </AccordionSummary>
                    <AccordionDetails sx={{ p: 3 }}>
                      { items.filter((item) => item.teamId === teamId && item.reviewRequest).length === 0 ?
                      'No review requests for this team' :
                        <TableContainer>
                          <Table aria-label="simple table">
                            <TableHead>
                              <TableRow>
                                <TableCell>Review request</TableCell>
                                <TableCell>Created on</TableCell>
                                <TableCell>Status</TableCell>
                                <TableCell>Link</TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {
                                items.filter((item) => item.teamId === teamId && item.reviewRequest)
                                    .map((item) => {
                                      return (
                                        <TableRow key={ item.id }>
                                          <TableCell>{ item.reviewRequest.title }</TableCell>
                                          <TableCell>
                                            {
                                              <Moment format="DD/MM/YYYY">
                                                {item.reviewRequest.createdOn}
                                              </Moment>
                                            }
                                          </TableCell>
                                          <TableCell>
                                            {
                                              Object.values(item.reviewers)
                                                  .find((value) => value === 'Rejected') ?
                                              `Item was rejected by 
                                              ${ Object.entries(item.reviewers)
                                                  .find(([_, status]) => status === 'Rejected')[0]}` :
                                              `Accepted by: 
                                              ${Object.values(item.reviewers)
                                                  .filter((status) => status === 'Accepted').length}` +
                                                  `/${Object.keys(item.reviewers).length} reviewers`
                                            }
                                          </TableCell>
                                          <TableCell>
                                            {
                                              <Button
                                                onClick={() => handleDetailsClick(item.id)}
                                              >
                                                View item
                                              </Button>
                                            }
                                          </TableCell>
                                        </TableRow>);
                                    })
                              }
                            </TableBody>
                          </Table>
                        </TableContainer>
                      }
                    </AccordionDetails>
                  </Accordion>
                );
              }) : null
            }
          </Box>
        </TabPanel>
      </TabContext>
    </Box>
  );
};
export default ViewReviewRequests;
