import { Box, Container } from '@mui/system';
import React, { useContext, useEffect, useState } from 'react';
import './CreateTeam.css';
import {
  Typography,
  TextField,
  Button,
  Autocomplete,
  Chip,
  Avatar,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useForm } from 'react-hook-form';
import { createTeam, getAllTeams } from '../../services/teams.services';
import { getAllUsers, updateUserTeams } from '../../services/users.services';
import AppContext from '../../providers/AppContext';
import { createNotification } from '../../services/notifications.services';
import notificationType from '../../enums/notification-type';
import { useNavigate } from 'react-router-dom';


const useStyles = makeStyles({
  input: {
    backgroundColor: 'white',
  },
});

export default function CreateTeam() {
  const { userData } = useContext(AppContext);
  const [members, setMembers] = useState([]);
  const [users, setUsers] = useState([]);
  const [avatarUrls, setAvatarUrls] = useState({});
  const navigate = useNavigate();
  const classes = useStyles();

  const { register, handleSubmit, setError, watch,
    formState: { errors } } = useForm({
    defaultValues: {
      username: '',
      email: '',
      phone: '',
    },
  });


  useEffect(() => {
    getAllUsers()
        .then((usersArr) => {
          const urlObj = usersArr.reduce((acc, el) => {
            acc[el.username] = el.avatarUrl;

            return acc;
          }, {});
          setAvatarUrls(urlObj);
          setUsers(usersArr);
        });
  }, []);

  const onSubmit = (data) => {
    getAllTeams()
        .then((teams) => {
          teams.map((team) => {
          // database validation
            if (team.teamName === data.teamName) {
              setError('teamName', {
                type: 'custom',
                message: 'Team name already taken.',
              });
              throw new Error('Team name taken');
            }
          });

          return createTeam(data.teamName, userData?.username, members)
              .then((response) => {
                updateUserTeams(userData?.username, data.teamName, response)
                    .catch(console.error);

                const teamObj = {
                  [response]: data.teamName,
                };
                const message = `${userData.username} has invited you
            to join team "${data.teamName}"`;
                members.map((member) =>
                  createNotification(notificationType.invitation, member,
                      teamObj, message, Date.now())
                      .catch(console.error),
                );
              })
              .then(navigate('../my-teams'))
              .catch(console.error);
        })
        .catch(console.error);
  };

  return (
    <Container
      className="CreateTeam"
      maxWidth="sm"
      sx={{ m: '5', position: 'relative' }}
    >
      <Typography
        variant="h4"
        component="h4"
        sx={{
          mt: '2rem',
          mb: '1rem',
          fontWeight: 400,
          color: 'inherit',
          textAlign: 'left',
        }}
      >
        Create a team
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
          }}
          mb={2}
        >
          <TextField
            inputProps={{ className: classes.input }}
            label="Team name"
            variant="outlined"
            required
            fullWidth
            error={!!errors?.teamName}
            helperText={errors?.teamName ? errors.teamName.message : null}
            {...register('teamName', {
              minLength: {
                value: 3,
                message: 'Team name is too short.',
              },
              maxLength: {
                value: 30,
                message: 'Team name is too long.',
              },
            })}
          />
        </Box>
        <Box mb={2}>
          <Autocomplete
            multiple
            options={users
                .filter((user) => user.username !== userData.username)
                .map((user) => user.username)}
            freeSolo
            onChange={(event, value) => setMembers(value)}
            renderTags={(value, getTagProps) =>
              value.map((option, index) => (
                <Chip key={index}
                  avatar={<Avatar src={avatarUrls[option]}/>}
                  variant="outlined"
                  label={option}
                  {...getTagProps({ index })}
                />
              ))
            }
            renderInput={(params) => (
              <TextField
                sx={{ backgroundColor: 'white' }}
                {...params}
                variant="outlined"
                label="Team members"
                placeholder="Add members"
              />
            )}
          />
        </Box>
        <Button
          type="submit"
          color="primary"
          variant="contained"
          size="large"
          disableElevation
          disabled={!watch('teamName') || members.length === 0}
        >
          Submit
        </Button>
      </form>
    </Container>
  );
}
