import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import { Button, Stack, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Logo from '../../images/quill-pen-solid.png';


export default function NavbarNotAuthenticated() {
  const navigate = useNavigate();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <img src={Logo} alt="" height={30} />
          <Typography
            variant="h5"
            noWrap
            component="div"
            sx={{ pl: 1, cursor: 'pointer', fontFamily: 'Satisfy', color: 'white' }}
            onClick={() => navigate('/')}
          >
            CollaboWriter
          </Typography>
          <Box sx={{ flexGrow: 1 }} />
          <Stack
            spacing={2}
            direction="row"
            sx={{ display: { xs: 'none', md: 'flex' } }}
          >
            <Button
              variant="contained"
              color="light"
              disableElevation
              onClick={() => navigate('signup')}
            >
              Sign Up
            </Button>
            <Button
              variant="outlined"
              color="light"
              onClick={() => navigate('login')}
            >
              Log In
            </Button>
          </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
