import React from 'react';
import './LoadingAnimaton.css';

const LoadingAnimation = () => {
  return (
    <div className="wrapper">
      <div className="dots">
        <div className="dot1"></div>
        <div className="dot2"></div>
        <div className="dot3"></div>
      </div>
    </div>
  );
};

export default LoadingAnimation;
