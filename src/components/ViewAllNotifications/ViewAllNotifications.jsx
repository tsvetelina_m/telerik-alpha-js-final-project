import React, { useContext, useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {
  ListSubheader,
  MenuItem,
  MenuList,
} from '@mui/material';
import Moment from 'react-moment';
import RateReviewIcon from '@mui/icons-material/RateReview';
import CommentIcon from '@mui/icons-material/Comment';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import ChangeCircleIcon from '@mui/icons-material/ChangeCircle';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import AppContext from '../../providers/AppContext';
import {
  acceptNotification,
  getUserNotifications,
  readNotifications,
} from '../../services/notifications.services';
import notificationType from '../../enums/notification-type';
import { getLiveNotificationsData }
  from '../../services/notifications.services';
import { respondTeamInvite } from '../../services/teams.services';
import { updateUserTeams } from '../../services/users.services';
import { useNavigate } from 'react-router-dom';

const ViewAllNotifications = () => {
  const [notifications, setNotifications] = useState([]);
  const { userData } = useContext(AppContext);
  const [read, setRead] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    if (!userData) {
      return;
    }

    const unsubscribe = getLiveNotificationsData(() => {
      getUserNotifications(userData.username)
          .then((snapshot) => {
            if (!snapshot.exists()) {
              return setNotifications([]);
            }
            setNotifications(Object.values(snapshot.val()).sort((a, b) => b.date - a.date));
          })
          .catch(console.error);
    });

    return () => unsubscribe();
  }, [userData]);

  useEffect(() => {
    readNotifications(read)
        .then(() => {
          setRead([]);
        })
        .catch(console.error);
  }, []);

  const handleCommentStatusClick = (notification) => {
    readNotifications([notification.id])
        .then(navigate(`../work-item/${Object.keys(notification.from)[0]}`))
        .catch(console.error);
  };

  const acceptInvite = (notification) => {
    const isAccepted = true;
    const [teamId, teamName] = Object.entries(notification.from)[0];
    setRead((prev) => [...prev, notification.id]);
    acceptNotification(notification.id, isAccepted)
        .catch(console.error);
    respondTeamInvite(teamId, userData.username, isAccepted)
        .catch(console.error);
    updateUserTeams(userData.username, teamName, teamId)
        .catch(console.error);
  };

  const declineInvite = (notification) => {
    const isAccepted = false;
    setRead((prev) => [...prev, notification.id]);
    acceptNotification(notification.id, isAccepted)
        .catch(console.error);
    respondTeamInvite(Object.keys(notification.from)[0],
        userData.username, isAccepted)
        .catch(console.error);
  };

  return (
    <>
      <MenuList
        sx={{ maxWidth: '70%', ml: 'auto', mr: 'auto' }}
        subheader={<ListSubheader
          sx={{ bgcolor: 'transparent', fontSize: 26, color: 'text.primary', mb: 4, textAlign: 'left' }}>
        Notifications
        </ListSubheader>}>
        {notifications.length === 0 ? (
            <MenuItem sx={{ mb: 1 }} >
              <ListItemText secondary="No notifications." />
            </MenuItem>
          ) : (
            notifications.map((notification) => {
              if (notification.type === notificationType.comment) {
                return (
                  <MenuItem
                    key={
                      notification.date +
                      Object.entries(notification.from)[0].join('')}
                    onClick={() =>
                      handleCommentStatusClick(notification)}
                  >
                    <ListItemIcon><CommentIcon /></ListItemIcon>
                    <ListItemText
                      primary="New comment"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                    <Moment fromNow>
                      {notification.date}
                    </Moment>
                  </MenuItem>
                );
              }

              if (notification.type === notificationType.reviewRequest) {
                return (
                  <MenuItem
                    key={notification.date + Object.entries(notification.from)[0].join('')}
                    onClick={() => handleCommentStatusReviewClick(notification)}
                  >
                    <ListItemIcon><RateReviewIcon /></ListItemIcon>
                    <ListItemText
                      primary="Review Request"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                    <Moment fromNow>
                      {notification.date}
                    </Moment>
                  </MenuItem>
                );
              }

              if (notification.type === notificationType.statusUpdate) {
                return (
                  <MenuItem
                    key={notification.data + Object.entries(notification.from)[0].join('')}
                    onClick={() => handleCommentStatusClick(notification)}
                  >
                    <ListItemIcon><ChangeCircleIcon/></ListItemIcon>
                    <ListItemText
                      primary="Status Change"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                    <Moment fromNow>
                      {notification.date}
                    </Moment>
                  </MenuItem>
                );
              }

              if (notification.type === notificationType.invitation) {
                return (
                  <MenuItem key={notification.id}>
                    <ListItemIcon><GroupAddIcon /></ListItemIcon>
                    <ListItemText
                      primary="Pending Team Invitation"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />

                    { notification.accepted === true ? (
                        <Typography color="green" sx={{ ml: 3 }}>
                        Accepted
                        </Typography>
                      ) :
                      notification.accepted === false ? (
                        <Typography color="red" sx={{ ml: 3 }}>
                          Declined
                        </Typography>
                      ) :
                      <Box sx={{ ml: 3 }}>
                        <IconButton
                          onClick={() => acceptInvite(notification)}
                          color="primary"
                        >
                          <CheckIcon />
                        </IconButton>
                        <IconButton
                          onClick={() => declineInvite(notification)}
                          color="primary"
                        >
                          <CloseIcon />
                        </IconButton>
                      </Box>
                    }
                  </MenuItem>
                );
              }

              return null;
            })
          )}
      </MenuList>
    </>
  );
};

export default ViewAllNotifications;
