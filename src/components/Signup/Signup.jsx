import React, { useState } from 'react';
import {
  TextField,
  Container,
  Button,
  Box,
  InputAdornment,
  Collapse,
  Alert,
  IconButton,
} from '@mui/material';
import { useForm } from 'react-hook-form';
import {
  createUserByUsername,
  getAllUsers,
} from '../../services/users.services';
import { registerUser } from '../../services/auth.services';
import { USER_VALIDATORS } from '../../constants/constants';
import { sendEmailVerification } from 'firebase/auth';
import Close from '@mui/icons-material/Close';

export default function Signup({ error }) {
  const {
    register,
    reset,
    watch,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm({
    defaultValues: {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      phone: '',
    },
  });
  const [openAlert, setOpenAlert] = useState(false);

  const onSubmit = (data) => {
    getAllUsers()
        .then((users) => {
          users.forEach((user) => {
          // database validation
            switch (true) {
              case user.username === data.username:
                setError('username', {
                  type: 'custom',
                  message: 'Username already in use.',
                });
                throw new Error('Username in use');

              case user.email === data.email:
                setError('email', {
                  type: 'custom',
                  message: 'E-mail address already in use.',
                });
                throw new Error('E-mail in use');

              case user.phone === data.phone:
                setError('phone', {
                  type: 'custom',
                  message: 'Phone number already in use.',
                });
                throw new Error('Phone in use');

              default:
                return true;
            }
          });

          return registerUser(data.email, data.password)
              .then((u) => {
                sendEmailVerification(u.user)
                    .then(setOpenAlert(true))
                    .catch(console.error);

                createUserByUsername(
                    data.username,
                    data.email,
                    data.firstName,
                    data.lastName,
                    data.phone,
                    u.user.uid,
                )
                    .catch(console.error);
              },
              )
              .then(reset())
              .catch(console.error);
        })
        .catch(console.error);
  };

  return (
    <Container sx={{ mt: 8 }} >
      <Box sx={{ flexGrow: 1, maxWidth: '60%', ml: 'auto', mr: 'auto' }}>
        <Collapse in={error&&openAlert}>
          <Alert
            severity="warning"
            action={
              <IconButton
                aria-label="Close"
                size="small"
                onClick={() => setOpenAlert(false)}
              >
                <Close fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 3 }}
          >
            Please follow the verification link sent to your e-mail, then you can refresh this page to continue.
          </Alert>
        </Collapse>
      </Box>
      <h1>Don&apos;t have an account yet? Please sign up!</h1>
      <Container maxWidth="xs">
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box mb={2}>
            <TextField
              label="E-mail"
              spellCheck="false"
              fullWidth
              required
              {...register('email', {
                pattern: {
                  value: USER_VALIDATORS.email,
                  message: 'Please enter a valid e-mail address',
                },
              })}
              error={!!errors?.email}
              helperText={errors?.email ? errors.email.message : null}
            />
          </Box>
          <Box mb={2}>
            <TextField
              label="Username"
              spellCheck="false"
              fullWidth
              required
              {...register('username', {
                minLength: { value: 2, message: 'Username too short.' },
                maxLength: { value: 20, message: 'Username too long.' },
              })}
              error={!!errors?.username}
              helperText={
                errors?.username ?
                  errors.username.message :
                  'Username must be between 2 and 20 characters.'
              }
            />
          </Box>
          <Box mb={2}>
            <TextField
              label="First name"
              spellCheck="false"
              fullWidth
              {...register('firstName')}
            />
          </Box>
          <Box mb={2}>
            <TextField
              label="Last name"
              spellCheck="false"
              fullWidth
              {...register('lastName')}
            />
          </Box>
          <Box mb={2}>
            <TextField
              label="Phone"
              fullWidth
              required
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">+359 (0)</InputAdornment>
                ),
              }}
              {...register('phone', {
                pattern: {
                  value: USER_VALIDATORS.phone,
                  message:
                    'Phone must be a valid number (9 digits excluding 0).',
                },
                setValueAs: (v) => '+359' + v,
              })}
              error={!!errors?.phone}
              helperText={errors?.phone ? errors.phone.message : null}
            />
          </Box>
          <Box mb={2}>
            <TextField
              type={'password'}
              label="Password"
              fullWidth
              required
              {...register('password', {
                minLength: {
                  value: 6,
                  message: 'Password should be at least 6 characters.',
                },
              })}
              error={!!errors?.password}
              helperText={errors?.password ? errors.password.message : null}
            />
          </Box>
          <Box mb={2}>
            <TextField
              type={'password'}
              label="Confirm password"
              required
              fullWidth
              {...register('confirmPassword', {
                validate: (val) => {
                  if (watch('password') !== val) {
                    return 'Passwords do not match.';
                  }
                },
              })}
              error={!!errors?.confirmPassword}
              helperText={
                errors?.confirmPassword ? errors.confirmPassword.message : null
              }
            />
          </Box>
          <Button
            type={'submit'}
            disabled={
              !watch('password') ||
              !watch('email') ||
              !watch('phone') ||
              !watch('username') ||
              !watch('confirmPassword')
            }
            variant="contained"
            disableElevation
            color="primary"
            fullWidth
            sx={{ color: 'white' }}
          >
            Sign up
          </Button>
        </form>
      </Container>
    </Container>
  );
}
