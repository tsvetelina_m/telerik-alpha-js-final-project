import {
  Box,
  Typography,
  Avatar,
  Chip,
  TextField,
  Autocomplete,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Accordion,
} from '@mui/material';
import {
  AccordionDetails,
  AccordionSummary,
  TableContainer,
  TableBody,
  Button,
} from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import {
  getAllTeams,
  getLiveTeamsData,
  removeMember,
} from '../../services/teams.services';
import AppContext from '../../providers/AppContext';
import { getAllUsers, removeTeam } from '../../services/users.services';
import notificationType from '../../enums/notification-type';
import { createNotification } from '../../services/notifications.services';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import memberStatus from '../../enums/team-member-status';
import { createInvite } from '../../services/teams.services';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import { useNavigate } from 'react-router-dom';
import { AddBoxOutlined } from '@mui/icons-material';

export default function MyTeams() {
  const [teams, setTeams] = useState([]);
  const [users, setUsers] = useState([]);
  const [avatarUrls, setAvatarUrls] = useState({});
  const [members, setMembers] = useState([]);
  const [clicked, setClicked] = useState('');
  const [openInviteForm, setOpenInviteForm] = useState(false);
  const [reRender, setReRender] = useState(false);
  const { userData } = useContext(AppContext);
  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = getLiveTeamsData(() => {
      getAllTeams()
          .then((response) => {
            return response.filter((team) => team.members && Object.entries(team.members)
                .find(([user, status]) =>
                  user === userData.username &&
                  status !== memberStatus.invited));
          })
          .then(setTeams)
          .catch(console.error);
    });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    getAllUsers()
        .then((usersArr) => {
          const urlObj = usersArr.reduce((acc, el) => {
            acc[el.username] = el.avatarUrl;

            return acc;
          }, {});
          setAvatarUrls(urlObj);
          setUsers(usersArr);
        })
        .catch(console.error);
  }, []);

  const handleChipClick = (username) => {
    navigate(`../users/${username}`);
  };

  const handleAddClick = (teamName) => {
    setOpenInviteForm(true);
    setClicked(teamName);
  };

  const submitInvites = (teamId) => {
    createInvite(members, teamId)
        .catch(console.error);

    const fromObj = {
      [teamId]: clicked,
    };
    const message =
      `${userData.username} has invited you to join team "${clicked}"`;
    members
        .map((member) =>
          createNotification(
              notificationType.invitation,
              member,
              fromObj,
              message,
              Date.now(),
          )
              .catch(console.error),
        );
    setOpenInviteForm(false);
    setReRender((prev) => !prev);
    setMembers([]);
  };

  const handleLeaveClick = (teamId) => {
    removeMember(teamId, userData.username)
        .catch(console.error);
    removeTeam(teamId, userData.username)
        .catch(console.error);
  };

  return (
    <Box sx={{ width: '100%', maxWidth: '1180px', ml: 'auto',
      mr: 'auto' }}>
      <Typography
        variant="h4"
        component="h4"
        sx={{
          mt: 2,
          mb: 2,
          fontWeight: 400,
          color: 'inherit',
          textAlign: 'left',
        }}
      >
        My teams
      </Typography>
      <Box>
        {teams.map((team) => {
          return (
            <Accordion key={team.id}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography>{team.teamName}</Typography>
              </AccordionSummary>
              <AccordionDetails sx={{ p: 3 }}
              >
                {
                  openInviteForm && clicked === team.teamName ?
                  <Autocomplete sx={{ m: '1rem' }}
                    key={reRender + team.id}
                    multiple
                    onBlur={() => submitInvites(team.id)}
                    options={
                      users
                          .filter((u) => !Object.keys(team.members)
                              .includes(u.username))
                          .map((user) => user.username)}
                    freeSolo
                    onChange={(event, value) => setMembers(value)}
                    renderTags={(value, getTagProps) =>
                      value.map((option, index) => (
                        <Chip
                          key={index}
                          avatar={<Avatar src={avatarUrls[option]}></Avatar>}
                          variant="outlined"
                          label={option}
                          {...getTagProps({ index })}
                        />
                      ))
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        label="Team members"
                        placeholder="Add members"
                      />
                    )}
                  /> : null
                }
                <Box sx={{ display: 'flex' }}>
                  <Button
                    startIcon={<AddBoxOutlined />}
                    onClick={() => handleAddClick(team.teamName)}>
                  Invite others
                  </Button>
                  <Button sx={{ ml: 2 }}
                    startIcon={<ExitToAppIcon />}
                    onClick={() => handleLeaveClick(team.id)}>
                  Leave team
                  </Button>
                </Box>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell>User</TableCell>
                        <TableCell>Role</TableCell>
                        <TableCell>Status</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {
                        Object.entries(team.members)
                            .map((member) => {
                              return (
                                <TableRow key={ member[0] }>
                                  <TableCell>
                                    { <Chip
                                      onClick={() => handleChipClick(member[0])}
                                      label={member[0]}
                                      avatar={<Avatar src={avatarUrls[member[0]]}/>}
                                    />
                                    }
                                  </TableCell>
                                  <TableCell>
                                    {member[1] === memberStatus.owner ?
                                    'Owner' :
                                      'Member'}
                                  </TableCell>
                                  <TableCell>
                                    {member[1] === memberStatus.invited ?
                                    'Invited' :
                                      'Joined'}
                                  </TableCell>
                                </TableRow>);
                            })
                      }
                    </TableBody>
                  </Table>
                </TableContainer>
              </AccordionDetails>
            </Accordion>
          );
        },
        )}
      </Box>
    </Box>
  );
}
