import React, { useContext, useState } from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {
  Alert,
  Avatar,
  Button,
  Collapse,
  IconButton,
  Input,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import {
  getAllUsers,
  getUserData,
  updateProfilePicture,
} from '../../services/users.services';
import AppContext from '../../providers/AppContext';
import {
  getDownloadURL,
  ref,
  uploadBytes,
  deleteObject,
} from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import { updateUserData } from '../../services/users.services';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { USER_VALIDATORS } from '../../constants/constants';
import {
  getAuth,
  EmailAuthProvider,
  updatePassword,
  reauthenticateWithCredential,
  updateEmail,
} from 'firebase/auth';
import PasswordDialog from '../PasswordDialog/PasswordDialog';

const Profile = () => {
  const { user, userData, setContext } = useContext(AppContext);
  const [openAlert, setOpenAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [openPasswordDialog, setOpenPasswordDialog] = useState(false);
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState({
    value: '',
    lengthError: false,
    matchError: false,
    errorMessage: '',
  });
  const [form, setForm] = useState({
    firstName: {
      value: userData.firstName || '',
      error: false,
    },
    lastName: {
      value: userData.lastName || '',
      error: false,
    },
    email: {
      value: userData.email,
      error: false,
    },
    phone: {
      value: userData.phone,
      error: false,
    },
  });

  const handleUploadPhoto = (e) => {
    e.preventDefault();

    const allowedFileTypes = ['image/jpeg', 'image/png'];
    const maxSizeInBytes = 2 * 1e6;
    const file = e.target?.files?.[0];

    if (!allowedFileTypes.includes(file.type)) {
      setAlertMessage('Please select a valid file!');
      setOpenAlert(true);

      return;
    }

    if (maxSizeInBytes < file.size) {
      setAlertMessage('The size of the selected file exceeds 2MB!');
      setOpenAlert(true);

      return;
    }

    const username = userData.username;

    const pictureRef = ref(storage, `images/${username}/avatar`);

    uploadBytes(pictureRef, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return updateProfilePicture(username, url).then(() => {
              setContext({
                user,
                userData: {
                  ...userData,
                  avatarUrl: url,
                },
              });
            });
          });
        })
        .catch(console.error);
  };

  const handleDeletePhoto = () => {
    const pictureRef = ref(storage, `images/${userData.username}/avatar`);

    deleteObject(pictureRef)
        .then(() => {
          setContext({
            user,
            userData: {
              ...userData,
              avatarUrl: '',
            },
          });
        })
        .catch(console.error);
  };

  const checkIfValid = (prop, value) => {
    return USER_VALIDATORS[prop].test(value);
  };

  const updateForm = (prop) => (e) => {
    const value = prop === 'phone' ? '+359' + e.target.value : e.target.value;
    let error = false;
    if (prop === 'phone' || prop === 'email') {
      error = !checkIfValid(prop, value);
    }

    setForm({
      ...form,
      [prop]: {
        error: error,
        value: prop === 'phone' ? `+359${e.target.value}` : e.target.value,
      },
    });
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const checkIfInUse = async (prop, value) => {
    const users = await getAllUsers();

    return users.some((user) => user[prop] === value);
  };

  const reauthenticateAndUpdateEmail = (password) => {
    if (password !== '') {
      const auth = getAuth();
      const user = auth.currentUser;
      const credential = EmailAuthProvider.credential(user.email, password);

      return reauthenticateWithCredential(user, credential)
          .then(() => {
            updateEmail(auth.currentUser, form.email.value)
                .then(() => {
                  update();
                })
                .catch(() => {
                  setAlertMessage('Email update failed.');
                  setOpenAlert(true);
                });
          })
          .catch((error) => {
            if (error.message.includes('auth/wrong-password')) {
              setAlertMessage(
                  'The password you entered is incorrect. Please try again.',
              );
              setOpenAlert(true);
            } else {
              console.error(error);
            }
          });
    }
  };

  const update = () => {
    updateUserData(
        userData.username,
        form.email.value,
        form.firstName.value,
        form.lastName.value,
        form.phone.value,
    )
        .then(() => {
          return getUserData(userData.uid).then((snapshot) => {
            if (snapshot.exists()) {
              setContext({
                user: user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
              });
            }
          });
        })
        .catch(console.error);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    let emailInUse = false;
    let phoneInUse = false;

    if (form.email.value !== userData.email) {
      emailInUse = await checkIfInUse('email', form.email.value);
    }

    if (form.phone.value !== userData.phone) {
      phoneInUse = await checkIfInUse('phone', form.phone.value);
    }

    if (emailInUse) {
      setAlertMessage('The E-mail is already in use.');
      setOpenAlert(true);

      return;
    }

    if (phoneInUse) {
      setAlertMessage('The phone number is already in use.');
      setOpenAlert(true);

      return;
    }

    if (form.email.value !== userData.email) {
      setOpenPasswordDialog(true);
    } else {
      update();
    }
  };

  const handleChangePassword = (e) => {
    e.preventDefault();

    if (
      currentPassword !== '' &&
      newPassword.value !== '' &&
      newPassword.lengthError === false &&
      newPassword.matchError === false
    ) {
      const auth = getAuth();
      const user = auth.currentUser;
      const credential = EmailAuthProvider.credential(
          user.email,
          currentPassword,
      );

      reauthenticateWithCredential(user, credential)
          .then(() => updatePassword(user, newPassword))
          .catch((error) => {
            if (error.message.includes('auth/wrong-password')) {
              setAlertMessage(
                  'The password you entered is incorrect. Please try again.',
              );
              setOpenAlert(true);
            } else {
              console.error(error);
            }
          });
    }
  };

  return (
    <Box sx={{ flexGrow: 1, maxWidth: '80%', ml: 'auto', mr: 'auto' }}>
      <Collapse in={openAlert}>
        <Alert
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpenAlert(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {alertMessage}
        </Alert>
      </Collapse>
      <PasswordDialog
        open={openPasswordDialog}
        setOpen={setOpenPasswordDialog}
        reauthenticateAndUpdateEmail={reauthenticateAndUpdateEmail}
      />
      <Typography variant="h4" component="h2" sx={{ textAlign: 'left', mb: 3 }}>
        Your Profile
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Paper
            sx={{
              height: 600,
              position: 'relative',
            }}
          >
            <Typography
              variant="h6"
              component="h3"
              sx={{
                textAlign: 'left',
                position: 'absolute',
                top: 20,
                left: 40,
              }}
            >
              Change Profile Picture
            </Typography>
            <Box
              sx={{
                height: 600,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                pl: 5,
                pr: 5,
              }}
            >
              <Avatar
                sx={{ width: 200, height: 200, mb: 4 }}
                src={userData.avatarUrl}
              ></Avatar>
              <Stack spacing={2} direction="row">
                <label htmlFor="upload-photo-btn">
                  <Input
                    sx={{ display: 'none' }}
                    id="upload-photo-btn"
                    type="file"
                    inputProps={{ accept: '.jpeg,.jpg,.png' }}
                    onChange={(e) => handleUploadPhoto(e)}
                  />
                  <Button
                    sx={{ color: 'white' }}
                    variant="contained"
                    component="span"
                    startIcon={<AddAPhotoIcon />}
                    disableElevation
                  >
                    Upload New
                  </Button>
                </label>
                <Button
                  sx={{ color: 'white' }}
                  variant="contained"
                  component="span"
                  startIcon={<DeleteIcon />}
                  onClick={handleDeletePhoto}
                  disableElevation
                >
                  Delete
                </Button>
              </Stack>
              <Typography variant="caption" component="p" sx={{ mt: 2 }}>
                Allowed file types: *.jpeg, *.jpg, *.png
              </Typography>
              <Typography variant="caption" component="p">
                Maximum file size: 2MB
              </Typography>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={8} sx={{ height: 600 }}>
          <Paper
            sx={{
              height: '50%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
              position: 'relative',
              pl: 5,
              pr: 5,
              mb: 2,
            }}
          >
            <form noValidate autoComplete="off">
              <Grid container rowSpacing={3} columnSpacing={3} sx={{ mb: 3 }}>
                <Grid item xs={12}>
                  <Typography
                    variant="h6"
                    component="h3"
                    sx={{
                      textAlign: 'left',
                    }}
                  >
                    Edit Profile
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    onChange={updateForm('firstName')}
                    label="First Name"
                    variant="outlined"
                    fullWidth
                    helperText=""
                    defaultValue={userData.firstName ? userData.firstName : ''}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    onChange={updateForm('lastName')}
                    label="Last Name"
                    variant="outlined"
                    fullWidth
                    helperText=""
                    defaultValue={userData.lastName ? userData.lastName : ''}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    onChange={updateForm('email')}
                    label="E-mail"
                    variant="outlined"
                    fullWidth
                    defaultValue={userData.email}
                    error={form.email.error}
                    helperText={
                      form.email.error ?
                        'Please enter a valid email address' :
                        ''
                    }
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    onChange={updateForm('phone')}
                    label="Phone"
                    variant="outlined"
                    fullWidth
                    defaultValue={userData.phone.split('+359')[1]}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          +359 (0)
                        </InputAdornment>
                      ),
                    }}
                    error={form.phone.error}
                    helperText={
                      form.phone.error ?
                        'Please enter a valid phone number' :
                        ''
                    }
                  />
                </Grid>
              </Grid>
              <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button
                  sx={{ color: 'white' }}
                  onClick={(e) => handleSubmit(e)}
                  type="submit"
                  color="primary"
                  variant="contained"
                  disableElevation
                >
                  Update Profile
                </Button>
              </Box>
            </form>
          </Paper>
          <Paper
            sx={{
              height: '50%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
              position: 'relative',
              pl: 5,
              pr: 5,
            }}
          >
            <Grid container rowSpacing={3} columnSpacing={3} sx={{ mb: 3 }}>
              <Grid item xs={12}>
                <Typography
                  variant="h6"
                  component="h3"
                  sx={{
                    textAlign: 'left',
                  }}
                >
                  Change Password
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Username"
                  variant="outlined"
                  fullWidth
                  helperText=""
                  disabled
                  defaultValue={userData.username}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  onChange={(e) => setCurrentPassword(e.target.value)}
                  type={showPassword ? 'text' : 'password'}
                  label="Current Password"
                  autoComplete="new-password"
                  variant="outlined"
                  fullWidth
                  helperText=""
                  defaultValue=""
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  onChange={(e) => {
                    if (e.target.value.length < 6) {
                      setNewPassword({
                        value: e.target.value,
                        lengthError: true,
                        matchError: false,
                        errorMessage:
                          'The password must be at least 6 characters long',
                      });
                    } else {
                      setNewPassword({
                        value: e.target.value,
                        lengthError: false,
                        matchError: false,
                        errorMessage: '',
                      });
                    }
                  }}
                  type={showPassword ? 'text' : 'password'}
                  label="New Password"
                  variant="outlined"
                  fullWidth
                  defaultValue=""
                  error={newPassword.lengthError}
                  helperText={
                    newPassword.lengthError ? newPassword.errorMessage : ''
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  onChange={(e) =>
                    e.target.value !== newPassword.value ?
                      setNewPassword({
                        ...newPassword,
                        matchError: true,
                        errorMessage: 'The passwords do not match!',
                      }) :
                      setNewPassword({
                        ...newPassword,
                        matchError: false,
                        errorMessage: '',
                      })
                  }
                  type={showPassword ? 'text' : 'password'}
                  label="Repeat New Password"
                  variant="outlined"
                  fullWidth
                  defaultValue=""
                  error={newPassword.matchError}
                  helperText={
                    newPassword.matchError ? newPassword.errorMessage : ''
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
            </Grid>
            <Button
              sx={{ color: 'white', ml: 'auto' }}
              onClick={(e) => handleChangePassword(e)}
              color="primary"
              variant="contained"
              disableElevation
            >
              Update Password
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Profile;
