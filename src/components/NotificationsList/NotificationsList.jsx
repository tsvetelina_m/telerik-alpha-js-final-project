import React, { useContext, useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {
  Button,
  ListSubheader,
  Menu,
  MenuItem,
  MenuList,
  Tooltip,
} from '@mui/material';
import { Badge } from '@mui/material';
import CommentIcon from '@mui/icons-material/Comment';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import ChangeCircleIcon from '@mui/icons-material/ChangeCircle';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import RateReviewIcon from '@mui/icons-material/RateReview';
import AppContext from '../../providers/AppContext';
import {
  acceptNotification,
  getUserNotifications,
  readNotifications,
} from '../../services/notifications.services';
import notificationType from '../../enums/notification-type';
import { getLiveNotificationsData }
  from '../../services/notifications.services';
import { respondTeamInvite } from '../../services/teams.services';
import { updateUserTeams } from '../../services/users.services';
import { useNavigate } from 'react-router-dom';
import { NotificationsOutlined } from '@mui/icons-material';

const NotificationsList = () => {
  const [anchorElNotifications, setAnchorElNotifications] = useState(null);
  const [notifications, setNotifications] = useState([]);
  const { userData } = useContext(AppContext);
  const [read, setRead] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    if (!userData) {
      return;
    }

    const unsubscribe = getLiveNotificationsData(() => {
      getUserNotifications(userData.username)
          .then((snapshot) => {
            if (!snapshot.exists()) {
              return setNotifications([]);
            }
            setNotifications(Object.values(snapshot.val()));
          })
          .catch(console.error);
    });

    return () => unsubscribe();
  }, [userData]);

  const handleViewAllClick = () => {
    navigate('notifications');
    setAnchorElNotifications(null);
  };

  const handleOpenNotifications = (event) => {
    setAnchorElNotifications(event.currentTarget);
  };

  const handleCloseNotifications = () => {
    readNotifications(read)
        .then(() => {
          setRead([]);
        })
        .catch(console.error);
    setAnchorElNotifications(null);
  };

  const handleCommentStatusReviewClick = (notification) => {
    readNotifications([notification.id])
        .then(navigate(`work-item/${Object.keys(notification.from)[0]}`))
        .catch(console.error);
    setAnchorElNotifications(null);
  };

  const acceptInvite = (notification) => {
    const isAccepted = true;
    const [teamId, teamName] = Object.entries(notification.from)[0];
    setRead((prev) => [...prev, notification.id]);
    acceptNotification(notification.id, isAccepted)
        .catch(console.error);
    respondTeamInvite(teamId, userData.username, isAccepted)
        .catch(console.error);
    updateUserTeams(userData.username, teamName, teamId)
        .catch(console.error);
  };

  const declineInvite = (notification) => {
    const isAccepted = false;
    setRead((prev) => [...prev, notification.id]);
    acceptNotification(notification.id, isAccepted)
        .catch(console.error);
    respondTeamInvite(Object.keys(notification.from)[0],
        userData.username, isAccepted)
        .catch(console.error);
  };

  return (
    <>
      <Tooltip title="Show notifications">
        <IconButton
          onClick={handleOpenNotifications}
          sx={{ p: 0.6 }}
          size="large"
          aria-label="show X new notifications"
          color="inherit"
        >
          <Badge
            sx={{
              '& .MuiBadge-badge': {
                color: 'white',
              },
            }}
            badgeContent={notifications.filter((n) => n.read !== true).length}
            color="primary"
          >
            <NotificationsOutlined/>
          </Badge>
        </IconButton>
      </Tooltip>
      <Menu
        sx={{ mt: '40px' }}
        id="notifications-appbar"
        anchorEl={anchorElNotifications}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElNotifications)}
        onClose={handleCloseNotifications}
      >
        <MenuList subheader={<ListSubheader sx={{ fontSize: 18 }}>Notifications</ListSubheader>}>
          {notifications.filter((n) => n.read !== true).length === 0 ? (
            <MenuItem sx={{ mb: 1 }} onClick={handleCloseNotifications}>
              <ListItemText secondary="No new notifications." />
            </MenuItem>
          ) : (
            notifications.filter((n) => n.read !== true).map((notification) => {
              if (notification.type === notificationType.comment) {
                return (
                  <MenuItem
                    key={
                      notification.date +
                      Object.entries(notification.from)[0].join('')}
                    onClick={() =>
                      handleCommentStatusReviewClick(notification)}
                  >
                    <ListItemIcon><CommentIcon /></ListItemIcon>
                    <ListItemText
                      primary="New comment"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                  </MenuItem>
                );
              }
              if (notification.type === notificationType.reviewRequest) {
                return (
                  <MenuItem
                    key={notification.date + Object.entries(notification.from)[0].join('')}
                    onClick={() => handleCommentStatusReviewClick(notification)}
                  >
                    <ListItemIcon><RateReviewIcon /></ListItemIcon>
                    <ListItemText
                      primary="Review Request"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                  </MenuItem>
                );
              }

              if (notification.type === notificationType.statusUpdate) {
                return (
                  <MenuItem
                    key={notification.date + Object.entries(notification.from)[0].join('')}
                    onClick={() => handleCommentStatusReviewClick(notification)}
                  >
                    <ListItemIcon><ChangeCircleIcon/></ListItemIcon>
                    <ListItemText
                      primary="Status Change"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />
                  </MenuItem>
                );
              }

              if (notification.type === notificationType.invitation) {
                return (
                  <MenuItem key={notification.id}>
                    <ListItemIcon><GroupAddIcon /></ListItemIcon>
                    <ListItemText
                      primary="Pending Team Invitation"
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {notification.message}
                          </Typography>
                        </React.Fragment>
                      }
                    />

                    { notification.accepted === true ? (
                      <Typography color="#00c292" sx={{ ml: 3 }}>
                        Accepted
                      </Typography>
                      ) :
                      notification.accepted === false ? (
                        <Typography color="#e46a76" sx={{ ml: 3 }}>
                          Declined
                        </Typography>
                      ) :
                      <Box sx={{ ml: 3 }}>
                        <IconButton
                          onClick={() => acceptInvite(notification)}
                          color="primary"
                        >
                          <CheckIcon />
                        </IconButton>
                        <IconButton
                          onClick={() => declineInvite(notification)}
                          color="primary"
                        >
                          <CloseIcon />
                        </IconButton>
                      </Box>
                    }
                  </MenuItem>
                );
              }

              return null;
            })
          )}
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', mt: 2 }}>
            <Button
              onClick={handleViewAllClick}
              size="small"
            >
            view all
            </Button>
          </Box>
        </MenuList>
      </Menu>
    </>
  );
};

export default NotificationsList;
