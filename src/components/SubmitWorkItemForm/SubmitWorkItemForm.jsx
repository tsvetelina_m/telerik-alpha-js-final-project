import React, { useContext, useState } from 'react';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  FormHelperText,
  Input,
  Box,
  Container,
  Typography,
  Chip,
  Alert,
  Collapse,
  IconButton,
} from '@mui/material';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from 'firebase/storage';
import CloseIcon from '@mui/icons-material/Close';
import TextEditor from '../TextEditor/TextEditor';
import {
  addAttachmentToWorkItem,
  addFileToWorkItem,
  addWorkItem,
} from '../../services/work-items.services';
import { storage } from '../../config/firebase-config';
import { useNavigate } from 'react-router-dom';
import { v4 } from 'uuid';
import AppContext from '../../providers/AppContext';
import {
  AttachFileOutlined,
  InsertDriveFileOutlined,
} from '@mui/icons-material';

const SubmitWorkItemForm = () => {
  const { userData } = useContext(AppContext);
  const [teamId, setTeamId] = useState('');
  const [teamName, setTeamName] = useState('');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [titleError, setTitleError] = useState(false);
  const [descriptionError, setDescriptionError] = useState(false);
  const [teamError, setTeamError] = useState(false);
  const [fileUpload, setFileUpload] = useState(null);
  const [attachmentUpload, setAttachmentUpload] = useState({});
  const [openAlert, setOpenAlert] = useState(false);

  const navigate = useNavigate();

  const handleTeamChange = (e) => {
    if (e.target.value === 'no-team') {
      console.log(1);
      setTeamId('');
      setTeamName('');

      return;
    }
    setTeamName(userData.teams[e.target.value]);

    return setTeamId(e.target.value);
  };

  const isInputValid = () => {
    let isValid = true;

    if (title.length <= 10 || title.length >= 80) {
      setTitleError(true);
      isValid = false;
    }

    if (description.length < 20 && fileUpload === null) {
      setDescriptionError(true);
      isValid = false;
    }

    if (description !== '' && fileUpload !== null) {
      setOpenAlert(true);
      isValid = false;
    }

    if (teamId === '') {
      setTeamError(true);
      isValid = false;
    }

    return isValid;
  };

  const handleFileSelect = (e) => {
    const file = e.target.files?.[0];

    if (
      file.type ===
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
      file.type === 'application/msword' ||
      file.type === 'application/pdf'
    ) {
      setFileUpload(file);
    }
  };

  const handleAttachmentSelect = (e) => {
    const attachments = Array
        .from(e.target.files)
        .reduce((acc, file) => {
          const attachmentId = v4();
          acc[attachmentId] = file;

          return acc;
        }, {});

    setAttachmentUpload((prev) => ({ ...prev, ...attachments }));
  };

  const uploadFile = (file, id) => {
    if (!file) return;

    const descriptionFile = storageRef(storage, `workItems/${id}/description`);

    uploadBytes(descriptionFile, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return addFileToWorkItem(id, url);
          });
        })
        .catch(console.error);
  };

  const uploadAttachment = (file, id, attachmentId) => {
    if (!file) return;

    const attachmentRef = storageRef(
        storage,
        `workItems/${id}/attachments/${attachmentId}`,
    );

    uploadBytes(attachmentRef, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return addAttachmentToWorkItem(id, url, attachmentId, file.name);
          });
        })
        .catch(console.error);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setTitleError(false);
    setDescriptionError(false);
    setTeamError(false);

    if (isInputValid() === true) {
      addWorkItem(title, description, teamId, teamName, userData.username)
          .then((item) => {
            if (fileUpload) {
              uploadFile(fileUpload, item.id);
            }

            if (Object.keys(attachmentUpload).length !== 0) {
              Object.entries(attachmentUpload).forEach(([key, file]) =>
                uploadAttachment(file, item.id, key),
              );
            }
            navigate(`../work-item/${item.id}`);
          })
          .catch(console.error);
    }
  };

  return (
    <Container
      className="SubmitWorkItemForm"
      sx={{ m: '5', mt: '2', position: 'relative', height: '100%' }}
    >
      <Collapse in={openAlert}>
        <Alert
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpenAlert(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mt: 2 }}
        >
          You cannot upload a file and submit text at the same time!
        </Alert>
      </Collapse>
      <Typography
        variant="h4"
        component="h4"
        sx={{
          mb: '1rem',
          fontWeight: 400,
          color: 'inherit',
          textAlign: 'left',
        }}
      >
        Add Work Item
      </Typography>
      <form noValidate autoComplete="off">
        <Box mb={2}>
          <FormControl
            fullWidth
            error={titleError}
          >
            <TextField
              sx={{ bgcolor: 'white' }}
              onChange={(e) => setTitle(e.target.value)}
              label="Title"
              variant="outlined"
              required
              error={titleError}
            />
            <FormHelperText>
              {titleError ?
                'The title should be between 10 and 80 characters' :
                ''}
            </FormHelperText>
          </FormControl>
        </Box>
        <Box
          mb={2}
          sx={{
            mt: '1rem',
            mb: '1rem',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <Typography
            variant="body1"
            component="p"
            sx={{
              display: 'inline-block',
              fontWeight: 400,
              color: 'inherit',
              textAlign: 'left',
            }}
          >
            Enter the text of the work item below OR select a file for upload:
          </Typography>
        </Box>
        <FormControl
          error={descriptionError}
          sx={{ width: '100%', height: 300 }}
        >
          <TextEditor setDescription={setDescription} />
          <FormHelperText>
            {descriptionError ?
              'The text should be at least 20 characters long or a file should be selected' :
              ''}
          </FormHelperText>
        </FormControl>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            textAlign: 'left',
          }}
        >
          <FormControl sx={{ minWidth: 200, mt: 2 }} error={teamError}>
            <InputLabel id="team-select-label">Team *</InputLabel>
            <Select
              sx={{ bgcolor: 'white' }}
              onChange={(e) => handleTeamChange(e)}
              labelId="team-select-label"
              id="team-select"
              value={teamId}
              label="Team *"
            >
              <MenuItem value="no-team">&nbsp;
              </MenuItem>
              {userData && Object
                  .entries(userData.teams)
                  .map(([id, name]) =>
                    <MenuItem value={id} key={id}>{name}</MenuItem>,
                  ) }
            </Select>
            <FormHelperText>
              {teamError ? 'Please select a team' : ''}
            </FormHelperText>
          </FormControl>
        </Box>
        <Box
          sx={{
            mt: '1rem',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <label htmlFor="contained-button-file">
            <Input
              sx={{ display: 'none' }}
              id="contained-button-file"
              type="file"
              inputProps={{ accept: '.doc,.docx,.pdf' }}
              onChange={(e) => handleFileSelect(e)}
            />
            <Button
              variant="text"
              component="span"
              disableElevation
              startIcon={<InsertDriveFileOutlined />}
            >
              select text file
            </Button>
          </label>
          {fileUpload ? (
            <Chip
              color="primary"
              variant="outlined"
              sx={{ ml: 2 }}
              label={fileUpload.name}
              onDelete={() => setFileUpload(null)}
            />
          ) : null}
        </Box>
        <Box sx={{ textAlign: 'left', mb: 3, mt: 2 }}>
          <label htmlFor="button-attachments">
            <Input
              sx={{ display: 'none' }}
              id="button-attachments"
              type="file"
              inputProps={{ multiple: true }}
              onChange={(e) => handleAttachmentSelect(e)}
            />
            <Button
              variant="text"
              component="span"
              disableElevation
              startIcon={<AttachFileOutlined />}
            >
              select attachments
            </Button>
          </label>
          {Object.keys(attachmentUpload).length !== 0 ?
            Object.entries(attachmentUpload).map(([key, file]) => {
              return (
                <Chip
                  key={key}
                  color="primary"
                  variant="outlined"
                  sx={{ ml: 2 }}
                  label={file.name}
                  onDelete={() => {
                    const attachments = { ...attachmentUpload };
                    delete attachments[key];
                    setAttachmentUpload(attachments);
                  }}
                />
              );
            }) :
            null}
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'flex-start' }}>
          <Button
            onClick={(e) => handleSubmit(e)}
            type="submit"
            variant="contained"
            size="large"
            disableElevation
            sx={{ color: 'white' }}
          >
            Submit
          </Button>
          <Button
            type="text"
            color="primary"
            variant="outlined"
            size="large"
            sx={{ ml: '1rem' }}
            onClick={() => navigate('/')}
          >
            Cancel
          </Button>
        </Box>
      </form>
    </Container>
  );
};

export default SubmitWorkItemForm;
