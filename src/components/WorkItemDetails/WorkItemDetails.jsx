import {
  Box,
  Chip,
  Container,
  Typography,
  Button,
  ButtonGroup,
  Input,
} from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import ReviewsOutlinedIcon from '@mui/icons-material/ReviewsOutlined';
import ChangeCircleOutlinedIcon from '@mui/icons-material/ChangeCircleOutlined';
import CheckCircleOutlineOutlinedIcon
  from '@mui/icons-material/CheckCircleOutlineOutlined';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';
import {
  getWorkItemById,
  updateWorkItemStatus,
  addHistoryEntry,
  getLiveWorkItemData,
  updateWorkItemDescription,
  addFileToWorkItem,
} from '../../services/work-items.services';
import parse from 'html-react-parser';
import Moment from 'react-moment';
import workItemStatusEnum from '../../enums/work-item-status';
import CommentDialog from '../CommentDialog/CommentDialog';
import WorkItemHistory from '../WorkItemHistory/WorkItemHistory';
import AppContext from '../../providers/AppContext';
import ReviewRequestInfoBox from '../ReviewRequestInfoBox/ReviewRequestInfoBox';
import TextEditor from '../TextEditor/TextEditor';
import notificationType from '../../enums/notification-type';
import { createNotification, deleteItemNotifications } from '../../services/notifications.services';
import { InsertDriveFileOutlined } from '@mui/icons-material';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import { updateWorkItemAndReviewerStatus } from '../../services/work-items.services';
import { deleteWorkItemById } from '../../services/work-items.services';

const WorkItemDetails = () => {
  const { id } = useParams();
  const { userData } = useContext(AppContext);
  const [workItem, setWorkItem] = useState(null);
  const [openRejectDialog, setOpenRejectDialog] = useState(false);
  const [openRequestChangeDialog, setOpenRequestChangeDialog] = useState(false);
  const [openCommentDialog, setOpenCommentDialog] = useState(false);
  const [isInEditMode, setInEditMode] = useState(false);
  const [newDescription, setNewDescription] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = getLiveWorkItemData(id, () => {
      getWorkItemById(id).then(setWorkItem).catch(console.error);
    });

    return () => unsubscribe();
  }, [id]);

  const accordionHeader = {
    reviewer: `You have been asked to review this work item.
Click here for more information`,
    author: `You have requested review of this work item.
Click here for more information`,
  };

  const checkAndUpdateStatus = (newStatus) => {
    if (newStatus === workItemStatusEnum.Accepted) {
      if (Object.values(workItem.reviewers)
          .every((status) => status === workItemStatusEnum.Accepted)) {
        updateWorkItemStatus(id, newStatus).catch(console.error);
      }
    } else {
      updateWorkItemStatus(id, newStatus).catch(console.error);
    }
  };

  const handleClickChangeStatus = (newStatus) => {
    addHistoryEntry(id, userData.username, '', newStatus)
        .then(() => {
          if (newStatus === workItemStatusEnum.Accepted) {
            return updateWorkItemAndReviewerStatus(id, newStatus, userData.username)
                .catch(console.error);
          } else {
            return checkAndUpdateStatus(newStatus)
                .catch(console.error);
          }
        })
        .then(() => {
          const fromObj = {
            [workItem.id]: workItem.title,
          };
          const message = `Work item:"${workItem.title}" status was changed to "${newStatus}".`;
          const recipientsArr = [...Object.keys(workItem.reviewers), workItem.author]
              .filter((username) => username !== userData.username);

          recipientsArr.map( (username) => {
            return createNotification(
                notificationType.statusUpdate,
                username,
                fromObj,
                message,
                Date.now())
                .catch(console.error);
          });
        })
        .catch(console.error);
  };

  const handleClickReject = () => {
    setOpenRejectDialog(true);
  };

  const handleClickRequestChange = () => {
    setOpenRequestChangeDialog(true);
  };

  const handleClickComment = () => {
    setOpenCommentDialog(true);
  };

  const handleClickDeleteItem = (id) => {
    deleteWorkItemById(id)
        .then(() => deleteItemNotifications(id)
            .catch(console.error))
        .then(() => navigate('/dashboard'));
  };

  const handleClickRequestReview = () => {
    navigate('./request-review');
  };

  const handleClickEdit = () => {
    setNewDescription(workItem.description);
    setInEditMode(true);
  };

  const handleClickSaveChanges = () => {
    updateWorkItemDescription(id, newDescription)
        .then(() => addHistoryEntry(id, userData.username, '', workItemStatusEnum.Pending))
        .then(() => updateWorkItemStatus(id, workItemStatusEnum.Pending))
        .then(() => setInEditMode(false))
        .catch(console.error);
  };

  const handleFileUpload = (e) => {
    const file = e.target.files?.[0];

    if (
      file.type ===
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
      file.type === 'application/msword' ||
      file.type === 'application/pdf'
    ) {
      const descriptionFile = storageRef(storage, `workItems/${id}/description`);

      uploadBytes(descriptionFile, file)
          .then((snapshot) => {
            return getDownloadURL(snapshot.ref).then((url) => {
              return addFileToWorkItem(id, url);
            });
          })
          .catch(console.error);
    }
  };

  return workItem === null ? null : (
    <div>
      <Container
        className="WorkItemDetails"
        sx={{ mt: 1, textAlign: 'left', pb: 5 }}
      >
        {workItem.reviewRequest &&
          (<Box sx={{ mb: 3 }}>
            <ReviewRequestInfoBox
              header={workItem.author !== userData.username ?
              accordionHeader.reviewer :
              accordionHeader.author}
              reviewRequest={workItem.reviewRequest}
              reviewers={workItem.reviewers}
            />
          </Box>)}
        <Box sx={{ position: 'relative' }} >
          <Typography variant="h4" component="span">
            {workItem.title}
          </Typography>

          <Typography
            variant="body2"
            component="p"
            sx={{
              mb: '1rem',
            }}
          >
          submitted <Moment fromNow>{workItem?.createdOn}</Moment> by{' '}
            {workItem.author}
          </Typography>
          <Box
            sx={{
              position: 'absolute',
              top: 0,
              right: 0,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Typography
              variant="subtitle1"
              component="p"
              sx={{
                margin: '0 auto',
                mr: 4,
              }}
            >
            Team: {workItem.teamName}
            </Typography>
            <Typography
              variant="subtitle1"
              component="p"
              sx={{
                margin: '0 auto',
                mr: 1,
              }}
            >
            Status:
            </Typography>
            <Chip label={workItem.status} color="primary" sx={{ color: 'white' }} />
          </Box>
        </Box>
        {workItem.fileUrl && (
          <div>
            <iframe
              title={workItem.title}
              height="550px"
              width="1180px"
              frameBorder="0"
              src={`https://docs.google.com/gview?url=${encodeURI(workItem.fileUrl)}&embedded=true`}
            />
          </div>
        )}
        {workItem.description && (
          isInEditMode ?
            <TextEditor
              setDescription={setNewDescription}
              initialValue={workItem.description}
              height={'400px'} /> :
          <Typography
            variant="body1"
            component="div"
            sx={{
              mt: '2rem',
              mb: '1rem',
              height: '400px',
              overflowY: 'scroll',
            }}
          >
            {parse(workItem.description)}
          </Typography>
        )}
        {Object.keys(workItem.attachments).length === 0 ? null : (
          <Box sx={{ display: 'flex', alignItems: 'center', mb: 2, mt: 2 }}>
            <Typography variant="body1" component="div">
              Attachments:{' '}
            </Typography>
            {Object.values(workItem.attachments).map((value) => {
              return (
                <Chip
                  component="a"
                  target="_blank"
                  href={value.url}
                  key={value.name}
                  color="primary"
                  variant="outlined"
                  sx={{ ml: 2 }}
                  label={value.name}
                  clickable
                  download
                />
              );
            })}
          </Box>
        )}
        {workItem.author === userData.username &&
        (<Box>
          <Button
            onClick={handleClickRequestReview}
            variant="text"
            sx={{ ml: 1 }}
          >
            Request Review
          </Button>

          {workItem.description ? (isInEditMode ? <Button
            onClick={handleClickSaveChanges}
            variant="text"
            sx={{ ml: 1 }}
          >
            Save Changes
          </Button> : <Button
            onClick={handleClickEdit}
            variant="text"
            sx={{ ml: 1 }}
          >
            Edit Item
          </Button>) : ( <Box
            sx={{
              mt: '1rem',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}
          >
            <label htmlFor="contained-button-file">
              <Input
                sx={{ display: 'none' }}
                id="contained-button-file"
                type="file"
                inputProps={{ accept: '.doc,.docx,.pdf' }}
                onChange={(e) => handleFileUpload(e)}
              />
              <Button
                variant="text"
                component="span"
                disableElevation
                startIcon={<InsertDriveFileOutlined />}
              >
              upload new version
              </Button>
            </label>
          </Box>)}
          <Button
            onClick={() => handleClickDeleteItem(workItem.id)}
            variant="text"
            sx={{ ml: 1 }}
          >
            Delete Item
          </Button>
        </Box>)
        }
        <Box sx={{ display: 'flex' }}>
          {workItem.author !== userData.username && workItem.reviewRequest &&
            <ButtonGroup
              disableElevation
              variant="outlined"
              aria-label="outlined button group"
            >
              <Button
                onClick={() =>
                  handleClickChangeStatus(workItemStatusEnum.UnderReview)
                }
                startIcon={<ReviewsOutlinedIcon />}
              >
              Under Review
              </Button>
              <Button
                onClick={handleClickRequestChange}
                startIcon={<ChangeCircleOutlinedIcon />}
              >
              Request Change
              </Button>
              <CommentDialog
                open={openRequestChangeDialog}
                setOpen={setOpenRequestChangeDialog}
                dialogText="Please, leave a comment with the changes
              you would like to request:"
                dialogTitle="Request Changes"
                id={id}
                username={userData.username}
                newStatus={workItemStatusEnum.ChangeRequested}
              ></CommentDialog>
              <Button
                onClick={() =>
                  handleClickChangeStatus(workItemStatusEnum.Accepted)
                }
                startIcon={<CheckCircleOutlineOutlinedIcon />}
              >
              Accept
              </Button>
              <Button
                onClick={handleClickReject}
                startIcon={<CancelOutlinedIcon />}
              >
              Reject
              </Button>
              <CommentDialog
                open={openRejectDialog}
                setOpen={setOpenRejectDialog}
                dialogText="Please, leave a comment
              explaining why the work item was rejected:"
                dialogTitle="Reject"
                id={id}
                username={userData.username}
                newStatus={workItemStatusEnum.Rejected}
              ></CommentDialog>
            </ButtonGroup>}
          <Button onClick={handleClickComment} variant="text" sx={{ ml: 1 }}>
            Add Comment
          </Button>
          <CommentDialog
            open={openCommentDialog}
            setOpen={setOpenCommentDialog}
            dialogText="Enter your comment below:"
            dialogTitle=""
            id={id}
            username={userData.username}
            newStatus=""
          ></CommentDialog>
        </Box>

        {Object.keys(workItem.history).length === 0 ?
          <Typography
            sx={{ textAlign: 'center', mt: 2 }}
            variant="body"
            component="div">
          There is no history or comments for this work item yet.
          </Typography> :
          <WorkItemHistory history={workItem.history} workItemAuthor={workItem.author} />}
      </Container>
    </div>
  );
};

export default WorkItemDetails;
