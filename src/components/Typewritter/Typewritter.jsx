import React from 'react';
import TypeWriterEffect from 'react-typewriter-effect';

const TypewriterText = () => {
  return (
    <TypeWriterEffect
      textStyle={{
        color: '#066a73',
        fontSize: 'inherit',
      }}
      startDelay={2000}
      cursorColor="#3F3D56"
      multiText={[
        'books.',
        'articles.',
        'blogs.',
        'lyrics.',
        'poems.',
        'texts.',
      ]}
      multiTextDelay={1000}
      typeSpeed={100}
    />
  );
};

export default TypewriterText;
