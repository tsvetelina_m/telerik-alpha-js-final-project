import React, { useContext } from 'react';
import { TextField, Container, Button, Box } from '@mui/material';
import { useForm } from 'react-hook-form';
import { loginUser } from '../../services/auth.services';
import { getUserData } from '../../services/users.services';
import AppContext from '../../providers/AppContext';
import { USER_VALIDATORS } from '../../constants/constants';
import { updateLastLogIn } from '../../services/users.services';

export default function Login() {
  const { userData, setContext } = useContext(AppContext);
  const {
    register,
    handleSubmit,
    watch,
    setError,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    loginUser(data.email, data.password)
        .then((u) => {
          return getUserData(u.user.uid).then((snapshot) => {
            if (snapshot.exists()) {
              setContext({
                user: u.user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
              });
            }
          });
        })
        .then(() => updateLastLogIn(userData.username, Date.now()))
        .catch((error) => {
          if (error.message.includes('wrong-password')) {
            setError('password', {
              type: 'custom',
              message: 'Incorrect password',
            });
          } else {
            console.error(error);
          }
        });
  };

  return (
    <Container sx={{ mt: 10 }}>
      <h1>Please log in before you continue browsing</h1>
      <Container maxWidth="xs">
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box mb={1}>
            <TextField
              name="email"
              label="E-mail"
              required
              fullWidth
              {...register('email', {
                pattern: {
                  value: USER_VALIDATORS.email,
                  message: 'Invalid e-mail address.',
                },
              })}
              error={!!errors?.email}
              helperText={errors?.email ? errors.email.message : null}
            />
          </Box>
          <Box mb={1}>
            <TextField
              type={'password'}
              label="Password"
              required
              fullWidth
              error={!!errors?.password}
              helperText={errors?.password ? errors.password.message : null}
              {...register('password')}
            />
          </Box>
          <Button
            sx={{ color: 'white' }}
            type={'submit'}
            variant="contained"
            color="primary"
            fullWidth
            disableElevation
            disabled={!watch('password') || !watch('email')}
          >
            Log in
          </Button>
        </form>
      </Container>
    </Container>
  );
}
