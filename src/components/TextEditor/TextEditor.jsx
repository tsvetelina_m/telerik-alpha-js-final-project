import React, { useRef } from 'react';
import { Editor } from '@tinymce/tinymce-react';

const TextEditor = ({ setDescription, initialValue = '', height = 300 }) => {
  const editorRef = useRef(null);

  const handleEditorChange = (e) => {
    setDescription(e.target.getContent());
  };

  return (
    <Editor
      onInit={(evt, editor) => (editorRef.current = editor)}
      initialValue={ initialValue }
      init={{
        height: { height },
        menubar: false,
        resize: false,
        selector: 'textarea',
        placeholder: 'Enter your text here...',
        skin: 'material-outline',
        content_css: 'material-outline',
        plugins: [
          'advlist',
          'autolink',
          'lists',
          'link',
          'image',
          'charmap',
          'preview',
          'anchor',
          'searchreplace',
          'visualblocks',
          'fullscreen',
          'insertdatetime',
          'media',
          'table',
          'help',
          'wordcount',
        ],
        toolbar:
          'undo redo | casechange blocks | bold italic backcolor | ' +
          'alignleft aligncenter alignright alignjustify | ' +
          'bullist numlist checklist outdent indent | removeformat | a11ycheck code table help',
        content_style:
          'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
      }}
      onChange={handleEditorChange}
    />
  );
};

export default TextEditor;
