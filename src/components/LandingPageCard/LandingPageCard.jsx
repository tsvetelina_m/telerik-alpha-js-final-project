import { Grid, Paper, Typography } from '@mui/material';
import React, { useState } from 'react';

const LandingPageCard = ({ icon, text }) => {
  const [elevation, setElevation] = useState(1);

  return (
    <Grid item xs={4}>
      <Paper
        onMouseOver={() => setElevation(5)}
        onMouseOut={() => setElevation(1)}
        elevation={elevation}
        sx={{ padding: '3rem', height: '70%' }}
      >
        <img src={icon} alt="" />
        <Typography
          variant="subtitle1"
          component="p"
          sx={{
            mt: '2.5rem',
            color: 'inherit',
            textAlign: 'center',
          }}
        >
          {text}
        </Typography>
      </Paper>
    </Grid>
  );
};

export default LandingPageCard;
