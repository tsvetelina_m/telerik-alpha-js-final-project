export const USER_VALIDATORS = {

  email: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
  phone: /^[0-9+]{13,13}$/i,
};
