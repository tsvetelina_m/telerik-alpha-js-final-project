const memberStatus = {

  invited: 'invited',
  owner: 'owner',
  member: 'member',
};

export default memberStatus;
