const workItemStatusEnum = {
  Pending: 'Pending',
  UnderReview: 'Under Review',
  ChangeRequested: 'Change Requested',
  Accepted: 'Accepted',
  Rejected: 'Rejected',
};

export default workItemStatusEnum;
