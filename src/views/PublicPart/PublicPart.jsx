import React from 'react';
import NavbarNotAuthenticated
  from '../../components/NavbarNotAuthenticated/NavbarNotAuthenticated';
import { Routes, Route } from 'react-router-dom';
import Signup from '../../components/Signup/Signup';
import Login from '../../components/Login/Login';
import LandingPage from '../../components/LandingPage/LandingPage';

const PublicPart = ({ error }) => {
  return (
    <div>
      <NavbarNotAuthenticated />
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="signup" element={<Signup error={error} />} />
        <Route path="login" element={<Login />} />
      </Routes>
    </div>
  );
};

export default PublicPart;
