import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from 'firebase/database';
import { db } from '../config/firebase-config';

export const getUsersCount = () => {
  return get(ref(db, 'users'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return 0;
        }

        const usersDocument = snapshot.val();

        return Object.keys(usersDocument).length;
      });
};

export const getAllUsers = () => {
  return get(ref(db, 'users'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        const usersDocument = snapshot.val();

        return Object.keys(usersDocument).map((key) => {
          const user = usersDocument[key];

          return {
            ...user,
          };
        });
      });
};
export const getUserByUsername = (username) => {
  return get(ref(db, `users/${username}`));
};

export const createUserByUsername =
  (username, email, firstName, lastName, phone, uid) => {
    return set(ref(db, `users/${username}`),
        { username, email, firstName, lastName, phone, uid, lastLogIn: Date.now() });
  };

export const updateUserData = (username, email, firstName, lastName, phone) => {
  return update(ref(db), {
    [`users/${username}/firstName`]: firstName,
    [`users/${username}/lastName`]: lastName,
    [`users/${username}/email`]: email,
    [`users/${username}/phone`]: phone,
  });
};


export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateUserTeams = (username, teamName, teamId) => {
  return update(ref(db), { [`/users/${username}/teams/${teamId}`]: teamName });
};

export const removeTeam = (teamId, username) => {
  return update(ref(db), { [`/users/${username}/teams/${teamId}`]: null });
};

export const updateProfilePicture = (username, url) => {
  return update(ref(db), {
    [`users/${username}/avatarUrl`]: url,
  });
};

export const updateLastLogIn = (username, time) => {
  return update(ref(db), {
    [`users/${username}/lastLogIn`]: time,
  });
};

export const getLiveUserData = (username, listen) => {
  return onValue(ref(db, `/users/${username}`), listen);
};
