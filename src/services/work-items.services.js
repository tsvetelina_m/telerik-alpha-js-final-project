import {
  ref,
  set,
  get,
  update,
  push,
  onValue,
  query,
  orderByChild,
  equalTo } from 'firebase/database';
import { db } from '../config/firebase-config';
import { v4 } from 'uuid';
import workItemStatusEnum from '../enums/work-item-status';

export const addWorkItem = (title, description, teamId, teamName, username) => {
  const id = v4();

  return set(
      ref(db, 'workItems/' + id),
      {
        id,
        title,
        description,
        teamId,
        teamName,
        author: username,
        status: workItemStatusEnum.Pending,
        createdOn: Date.now(),
      },
  )
      .then(() => {
        return getWorkItemById(id);
      });
};

export const getWorkItemById = (id) => {
  return get(ref(db, `workItems/${id}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Work item with id ${id} does not exist!`);
        }

        const workItem = result.val();
        workItem.id = id;
        workItem.createdOn = new Date(workItem.createdOn);
        workItem.history = workItem.history ? workItem.history : {};
        workItem.attachments = workItem.attachments ? workItem.attachments : {};
        workItem.reviewers = workItem.reviewers ? workItem.reviewers : {};
        if (workItem.reviewRequest) {
          workItem.reviewRequest.attachments =
            workItem.reviewRequest.attachments ?
            workItem.reviewRequest.attachments :
            {};
        }

        return workItem;
      });
};

export const getAllWorkItems = () => {
  return get(ref(db, 'workItems'))
      .then((result) => {
        if (!result.exists()) {
          return [];
        }

        const workItems = Object.values(result.val());
        workItems.map((workItem) => {
          workItem.reviewers = workItem.reviewers ? workItem.reviewers : {};

          return workItem;
        });

        return workItems;
      });
};

export const getWorkItemsByAuthor = (author) => {
  return get(query(ref(db, 'workItems'), orderByChild('author'), equalTo(author)))
      .then((result) => {
        if (!result.exists()) {
          return [];
        }

        return Object.values(result.val());
      });
};

export const deleteWorkItemById = (id) => {
  return update(ref(db), {
    [`workItems/${id}`]: null,
  });
};

export const addFileToWorkItem = (id, url) => {
  return update(ref(db), {
    [`workItems/${id}/fileUrl`]: url,
  });
};

export const addAttachmentToWorkItem = (id, url, attachmentId, name) => {
  return update(ref(db), {
    [`workItems/${id}/attachments/${attachmentId}/url`]: url,
    [`workItems/${id}/attachments/${attachmentId}/name`]: name,
  });
};

export const updateWorkItemStatus = (id, newStatus) => {
  const updateContent = {};
  updateContent[`workItems/${id}/status`] = newStatus;

  return update(ref(db), updateContent);
};

export const updateWorkItemAndReviewerStatus = (id, newStatus, reviewerUsername) => {
  const updateContent = {};
  updateContent[`workItems/${id}/status`] = newStatus;
  updateContent[`workItems/${id}/reviewers/${reviewerUsername}`] = newStatus;

  return update(ref(db), updateContent);
};

export const updateWorkItemDescription = (id, newDescription) => {
  const updateContent = {};
  updateContent[`workItems/${id}/description`] = newDescription;

  return update(ref(db), updateContent);
};

export const updateReviewers = (id, reviewers) => {
  const reviewersObject = reviewers.reduce((acc, cur) => {
    acc[cur] = 'Pending';

    return acc;
  }, {});

  return update(ref(db), {
    [`workItems/${id}/reviewers`]: reviewersObject,
  });
};

export const updateReviewerStatus = (id, newStatus, reviewerUsername) => {
  return update(ref(db), {
    [`workItems/${id}/reviewers/${reviewerUsername}`]: newStatus,
  });
};

export const addHistoryEntry = (id, author, comment = '', newStatus = '') => {
  return push(ref(db, `workItems/${id}/history`),
      {
        newStatus,
        author,
        comment,
        createdOn: Date.now(),
      },
  );
};

export const addAttachmentToReviewRequest = (workItemId, attachmentId, url, name) => {
  return update(ref(db), {
    [`workItems/${workItemId}/reviewRequest/attachments/${attachmentId}/url`]: url,
    [`workItems/${workItemId}/reviewRequest/attachments/${attachmentId}/name`]: name,
  });
};

export const addReviewRequest = (workItemId, title, description) => {
  return set(
      ref(db, `workItems/${workItemId}/reviewRequest/`),
      {
        title,
        description,
        createdOn: Date.now(),
      },
  )
      .then(() => {
        return getReviewRequest(workItemId);
      });
};

export const getReviewRequest = (workItemId) => {
  return get(ref(db, `workItems/${workItemId}/reviewRequest/`))
      .then((result) => {
        const reviewRequest = result.val();
        reviewRequest.createdOn = new Date(reviewRequest.createdOn);
        reviewRequest.attachments = reviewRequest.attachments ?
        reviewRequest.attachments :
        {};

        return reviewRequest;
      });
};

export const getLiveWorkItemData = (id, listen) => {
  return onValue(ref(db, `/workItems/${id}`), listen);
};
