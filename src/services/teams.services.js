import { get, set, ref, update, onValue } from 'firebase/database';
import { db } from '../config/firebase-config';
import { v4 } from 'uuid';
import memberStatus from '../enums/team-member-status';


export const getAllTeams = () => {
  return get(ref(db, 'teams'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        const teamsDocument = snapshot.val();

        return Object.keys(teamsDocument).map((key) => {
          const team = teamsDocument[key];

          return {
            ...team,
          };
        });
      });
};

export const createTeam = (teamName, owner, members) => {
  const id = v4();
  const membersObj = members.reduce((acc, member) => {
    return {
      ...acc,
      [member]: memberStatus.invited,
    };
  }, { [owner]: memberStatus.owner });


  return set(ref(db, `teams/${id}`), {
    teamName,
    members: membersObj,
    id })
      .then(() => {
        return id;
      });
};

export const createInvite = (members, teamId) => {
  return Promise.all(members.map( (member) => {
    return update(ref(db),
        { [`/teams/${teamId}/members/${member}`]: memberStatus.invited });
  }));
};

export const removeMember = (teamId, member) => {
  return update(ref(db), { [`/teams/${teamId}/members/${member}`]: null });
};

/**
  * Based on response updates given team in DB.
  * @param {string} fromTeam which team is the invite from
  * @param {string} username
  * @param {boolean} isAccepted response, can be true
for accept, or false for declined
  * @return {Promise}
  */

export const respondTeamInvite = (teamId, username, isAccepted) => {
  const updateContent = {};
  updateContent[`teams/${teamId}/members/${username}`] = isAccepted ?
    memberStatus.member :
    null;

  return update(ref(db), updateContent);
};
export const getTeamById = (id) => {
  return get(ref(db, `teams/${id}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Team with id ${id} does not exist!`);
        }

        return result.val();
      });
};

export const getLiveTeamsData = (listen) => {
  return onValue(ref(db, '/teams'), listen);
};
