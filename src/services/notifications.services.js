import {
  get,
  set,
  ref,
  update,
  query,
  orderByChild,
  equalTo,
  onValue,
} from 'firebase/database';
import { db } from '../config/firebase-config';
import { v4 } from 'uuid';

export const createNotification =
  (type, username, from = {}, message, date, read = false) => {
    const id = v4();

    return set(ref(db, `notifications/${id}`),
        { type, username, from, message, id, date, read });
  };

export const getAllNotifications = () => {
  return get(ref(db, 'notifications'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        const notificationsDocument = snapshot.val();

        return Object.keys(notificationsDocument).map((key) => {
          const notification = notificationsDocument[key];

          return {
            ...notification,
          };
        });
      });
};

export const getUserNotifications = (username) => {
  return get(query(ref(db, 'notifications'),
      orderByChild('username'), equalTo(username)));
};

export const readNotifications = (idArr) => {
  const updateContent = idArr.reduce((acc, id) => {
    acc[`notifications/${id}/read`] = true;

    return acc;
  }, {});

  return update(ref(db), updateContent);
};

export const acceptNotification = (id, boolean) => {
  return update(ref(db), {
    [`/notifications/${id}/accepted`]: boolean,
  });
};

export const deleteItemNotifications = (itemId) => {
  return getAllNotifications()
      .then((res) => res.filter((notification) => notification.from[itemId]))
      .then((res) => res.forEach((notification) => {
        update(ref(db), {
          [`notifications/${notification.id}`]: null,
        });
      }));
};

export const getLiveNotificationsData = (listen) => {
  return onValue(ref(db, `/notifications`), listen);
};
